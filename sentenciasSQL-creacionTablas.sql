SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema VuelaFacil_DataBase
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema VuelaFacil_DataBase
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `VuelaFacil_DataBase` DEFAULT CHARACTER SET utf8 ;
USE `VuelaFacil_DataBase` ;

-- -----------------------------------------------------
-- Table `mydb`.`CITY`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `VuelaFacil_DataBase`.`CITY` (
  `idCITY` INT NOT NULL COMMENT 'DANE city code',
  `CITY` VARCHAR(45) NULL COMMENT 'City name',
  PRIMARY KEY (`idCITY`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `VuelaFacil_DataBase`.`AIRPORT`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `VuelaFacil_DataBase`.`AIRPORT` (
  `idAIRPORT` VARCHAR(4) NOT NULL COMMENT 'IATA code of the airport',
  `AIRPORT` VARCHAR(45) NULL COMMENT 'Airport name',
  `CITY_idCITY` INT NOT NULL,
  PRIMARY KEY (`idAIRPORT`),
  INDEX `fk_AIRPORT_CITY1_idx` (`CITY_idCITY` ASC) VISIBLE,
  CONSTRAINT `fk_AIRPORT_CITY1`
    FOREIGN KEY (`CITY_idCITY`)
    REFERENCES `VuelaFacil_DataBase`.`CITY` (`idCITY`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Toda la información del aeropuerto.';


-- -----------------------------------------------------
-- Table `VuelaFacil_DataBase`.`ROUTE`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `VuelaFacil_DataBase`.`ROUTE` (
  `idROUTE` INT NOT NULL COMMENT 'Route code',
  `DEP_TIME` DATETIME NULL COMMENT 'Departure Time',
  `ARR_TIME` DATETIME NULL COMMENT 'Arrival time',
  `FLIGHT_TIME` DATETIME NULL,
  `AIRPORT_idAIRPORT` VARCHAR(4) NOT NULL,
  `CITY_idCITY` INT NOT NULL,
  PRIMARY KEY (`idROUTE`),
  INDEX `fk_ROUTE_AIRPORT1_idx` (`AIRPORT_idAIRPORT` ASC) VISIBLE,
  INDEX `fk_ROUTE_CITY1_idx` (`CITY_idCITY` ASC) VISIBLE,
  CONSTRAINT `fk_ROUTE_AIRPORT1`
    FOREIGN KEY (`AIRPORT_idAIRPORT`)
    REFERENCES `VuelaFacil_DataBase`.`AIRPORT` (`idAIRPORT`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ROUTE_CITY1`
    FOREIGN KEY (`CITY_idCITY`)
    REFERENCES `VuelaFacil_DataBase`.`CITY` (`idCITY`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `VuelaFacil_DataBase`.`RESTRICTION`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `VuelaFacil_DataBase`.`RESTRICTION` (
  `idRESTRICTION` INT NOT NULL COMMENT 'Restriction assign number',
  `RESTRICTION_DESC` VARCHAR(45) NULL COMMENT 'Short description of the restriction',
  PRIMARY KEY (`idRESTRICTION`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `VuelaFacil_DataBase`.`STATUS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `VuelaFacil_DataBase`.`STATUS` (
  `idSTATUS` INT NOT NULL COMMENT 'Route status',
  `STATUS_DESC` INT NULL COMMENT 'Status description',
  `RESTRICTION_idRESTRICTION` INT NOT NULL,
  PRIMARY KEY (`idSTATUS`),
  INDEX `fk_STATUS_RESTRICTION1_idx` (`RESTRICTION_idRESTRICTION` ASC) VISIBLE,
  CONSTRAINT `fk_STATUS_RESTRICTION1`
    FOREIGN KEY (`RESTRICTION_idRESTRICTION`)
    REFERENCES `VuelaFacil_DataBase`.`RESTRICTION` (`idRESTRICTION`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `VuelaFacil_DataBase`.`FLIGHT`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `VuelaFacil_DataBase`.`FLIGHT` (
  `idFLIGHT` INT NOT NULL COMMENT 'Flight number',
  `FLIGHT_DATE` DATETIME NULL COMMENT 'Date of the flight',
  `PAX_CAPACITY` INT NULL,
  `AVL_SEAT` INT NULL COMMENT 'Available seats',
  `RESERVED_SEATS` INT NULL,
  `STATUS_idSTATUS` INT NOT NULL,
  `ROUTE_idROUTE` INT NOT NULL,
  PRIMARY KEY (`idFLIGHT`),
  INDEX `fk_FLIGHT_STATUS1_idx` (`STATUS_idSTATUS` ASC) VISIBLE,
  INDEX `fk_FLIGHT_ROUTE1_idx` (`ROUTE_idROUTE` ASC) VISIBLE,
  CONSTRAINT `fk_FLIGHT_STATUS1`
    FOREIGN KEY (`STATUS_idSTATUS`)
    REFERENCES `VuelaFacil_DataBase`.`STATUS` (`idSTATUS`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_FLIGHT_ROUTE1`
    FOREIGN KEY (`ROUTE_idROUTE`)
    REFERENCES `VuelaFacil_DataBase`.`ROUTE` (`idROUTE`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `VuelaFacil_DataBase`.`USER`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `VuelaFacil_DataBase`.`USER` (
  `idUSER` INT NOT NULL COMMENT 'User code',
  `USER_NAME` VARCHAR(45) NULL COMMENT 'User Name',
  `USER_TYPE` VARCHAR(45) NULL COMMENT 'Type of user between single person or flight agency',
  PRIMARY KEY (`idUSER`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `VuelaFacil_DataBase`.`BOOKING`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `VuelaFacil_DataBase`.`BOOKING` (
  `idBOOKING` INT NOT NULL COMMENT 'Booking code',
  `SELL_TIME` DATETIME NULL,
  `FEES` DECIMAL(21,2) NULL,
  `BOOKING_DATE` DATETIME NULL,
  `SEAT_CLASS` VARCHAR(3) NULL,
  `USER_idUSER` INT NOT NULL,
  `FLIGHT_idFLIGHT` INT NOT NULL,
  PRIMARY KEY (`idBOOKING`),
  INDEX `fk_BOOKING_USER1_idx` (`USER_idUSER` ASC) VISIBLE,
  INDEX `fk_BOOKING_FLIGHT1_idx` (`FLIGHT_idFLIGHT` ASC) VISIBLE,
  CONSTRAINT `fk_BOOKING_USER1`
    FOREIGN KEY (`USER_idUSER`)
    REFERENCES `VuelaFacil_DataBase`.`USER` (`idUSER`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_BOOKING_FLIGHT1`
    FOREIGN KEY (`FLIGHT_idFLIGHT`)
    REFERENCES `VuelaFacil_DataBase`.`FLIGHT` (`idFLIGHT`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
